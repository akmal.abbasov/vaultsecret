package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/hashicorp/vault/api"
)

const (
	jwtPath = "/var/run/secrets/kubernetes.io/serviceaccount/token"
)

func authToVault(vaultAddress string, roleID string, secretID string) (string, error) {
	config := api.DefaultConfig()
	config.Address = vaultAddress

	client, err := api.NewClient(config)
	if err != nil {
		return "", err
	}

	auth, err := client.Logical().Write("auth/kubernetes/login", map[string]interface{}{
		"role": roleID,
		"jwt":  secretID,
	})
	if err != nil {
		return "", err
	}

	vaultToken, err := auth.TokenID()
	if err != nil {
		return "", err
	}

	return vaultToken, nil
}

func readSecretID() (string, error) {
	secretID, err := ioutil.ReadFile(jwtPath)
	if err != nil {
		return "", err
	}

	return string(secretID), nil
}

func bailout(message string) {
	log.Fatal(message)
	os.Exit(1)
}

func main() {
	vaultAddress := flag.String("vault-address", "https://127.0.0.1:8200", "vault cluster address")
	vaultRole := flag.String("vault-role", "", "vault role")
	flag.Parse()

	if *vaultRole == "" {
		bailout("Vault role is not given.")
	}
	log.Printf("Started with vault-address=%s and vault-role=%s\n", *vaultAddress, *vaultRole)

	// read secret id(service account JWT token)
	secretID, err := readSecretID()
	if err != nil {
		bailout(fmt.Sprintf("Error reading service account JWT from: %s", jwtPath))
	}
	log.Print("Successfully read service account JWT")

	// authenticate to vault
	vaultToken, err := authToVault(*vaultAddress, *vaultRole, secretID)
	if err != nil {
		bailout(err.Error())
	}
	log.Print("Successfully authenticated to vault")

	// export vault token as environment variable
	err = os.Setenv("VAULT_TOKEN", vaultToken)
	if err != nil {
		bailout(err.Error())
	}
}
